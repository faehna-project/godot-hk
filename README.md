# Godot Engine - Faehna Version

A modified version of the Godot game engine for developing Faehna.
It currently adds the Voxel Tools and Network Synchronizer modules.

Current Godot Version: 4.2
Modules Last Updated: December 24th, 2023

## Added Modules
- [Zylann - Voxel Tools (modules/voxel)](https://github.com/Zylann/godot_voxel)
- [GodotNetworking - Network Synchronizer (modules/network_synchronizer)](https://github.com/GodotNetworking/network_synchronizer)
